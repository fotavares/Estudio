﻿<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8" />
	<title>Estúdio Fotográfico </title>
	<link rel="stylesheet" href="conteudo/estilo.css" type="text/css" />  

	<script src="js/funcoes.js" type="text/javascript"></script>
</head>

<body>

    <div id="container">
    
		<?php include_once 'layout/topo.php' ?>
		<?php include_once 'layout/lateral.php' ?>
    	<span class="textorcamento"><p>Olá! Obrigado por confiar no nosso trabalho. Preencha o formulário abaixo e nos solicite um orçamento.</p></span>
 
		<div id="orcamento">
			<form name="formulario" id="idform" action="envio.php" method="post">
				<fieldset>
						<legend>Solicite seu orçamento</legend>
						   
						   <fieldset id="dadospessoais">
								<legend>Dados Pessoais</legend>
								<label>Nome: <input type="text" tabindex="1" id="idnome" name="nome" size="40" maxlength="40" required
									pattern="[A-Za-zà-üÀ-Ü\s]{3,50}" placeholder="Nome"/></label> 
								<br />
								<label>Email: <input type="email" tabindex="2" id="idemail" name="email" namesize="40" placeholder="Email"
								 maxlength="40" required/>  </label> <br />
								<label>Telefone: <input type="tel" tabindex="3" id="idtel" name="telefone" size="15" pattern="[0-9]{10,11}" placeholder="DDD + Telefone"
								 maxlength="15" />  </label> <br /> <br />
							</fieldset>
			
							<fieldset id="dadosevento">
								<legend>Dados do Evento</legend>
								<label>CEP: <input type="text" id="idcep" tabindex="4" name="cep" size="10" maxlength="10" onblur="atualizacep(this.value)" required
								  /></label> <br /> 
								<label>Logradouro: <input type="text" id="idlogradouro" name="logradouro" size="36" maxlength="40" disabled="disabled" />
								</label> <br /> 
								<label>Número: <input type="number" id="idnumero" tabindex="5" name="numero" required/></label>
								<label>Complemento: <input type="text" id="idcomplemento" tabindex="6" name="complemento" size="10"
								maxlength="20"/>  </label> <br /> 
								<label>Bairro: <input type="text" id="idbairro" name="bairro" size="27" maxlength="31" disabled="disabled"/></label>  <br/>
								<label>Cidade: <input type="text" id="idcidade" name="cidade" size="27" maxlength="31" disabled="disabled"/></label>  
								<label>UF:<input type="text" id="iduf" name="uf" size="2" maxlength="2" disabled="disabled"/>
								</label> <br /> 
								<label>Data do evento:<input type="date" id="iddata" name="uf" min="2015-01-01" required/>
								</label> <br />
								
							</fieldset>    
							
							<fieldset id="observacoes">
								<legend>Observações</legend>
								<textarea rows="7" cols="62" name="obs" tabindex="7">                           
								</textarea>                        
							</fieldset>  
                            
						<button type="submit" id="btsolicitar" tabindex="8">Solicitar</button>
				</fieldset>  
		</div>
		<?php include_once 'layout/footer.php' ?>
	</div>
</body>
</html>
