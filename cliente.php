﻿<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8" />
	<title>Estúdio Fotográfico - Área do cliente</title>
	<link rel="stylesheet" href="conteudo/estilo.css" type="text/css" />  

	  <link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
	  <script src="//code.jquery.com/jquery-1.10.2.js"></script>
	  <script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
	  <script src="js/cliente.js"></script>      
	
</head>

<body>

    <div id="container">

		<?php include_once 'layout/topo.php' ?>
		<?php include_once 'layout/lateral.php' ?>
		<?php include_once 'login/validarusuario.php' ?>
			
        <div id="conteudo">
			<form action="selecao.php" method="post">
				<?php   
				include_once 'conexao_bd.php';
				$sql = "select * from cliente_imagem where idcliente ='".$_SESSION["codigo"]."' ";
		
				$result = mysqli_query($con,$sql);
				
				while($row = mysqli_fetch_array($result)){
					echo "<div class='imagemCliente'>".
							"<img src = 'conteudo/cliente/".$row["path"]."' width='300' height='200'/><br>".
							"<input type='checkbox' name='cliente[]' value='".explode("/",$row["path"])[1]."'/><label for='".explode("/",$row["path"])[1]."'>".explode("/",$row["path"])[1]."</label>".
						 "</div>";
				}
				?>
				
				<fieldset style="width:100%;position:relative;float:left";>
						<legend>Seleção de imagens</legend>
						<?php 
							$data = date_create($_SESSION["data"]);
						?>
						<p>Cliente: <?php echo $_SESSION["nome"]?> - Data do Evento: <?php echo date_format($data,"d/m/Y")?> - <a href="login/sair.php">Sair da seleção</a></p>				
						<p>Imagens Selecionadas: <span id="lbSelecionadas"></span></p>				
						<p><button type="submit" value="Enviar seleção">Enviar</button>
				</fieldset>
			</form>
			<?php include_once 'layout/footer.php' ?>
		</div>
    </div>

</body>
</html>