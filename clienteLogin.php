﻿<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8" />
	<title>Estúdio Fotográfico - Área do cliente</title>
	<link rel="stylesheet" href="conteudo/estilo.css" type="text/css" />  
	
</head>

<body>

    <div id="container">

		<?php include_once 'layout/topo.php' ?>
		<?php include_once 'layout/lateral.php' ?>
			
        <div id="conteudo">
			<div id="loginBox">
				<h3>Área do cliente</h3>
				<form method="post" action="login/verificarlogin.php">
					<p>Usuário:<br/>
					<input type="text" name="usuario" required></p>
					<p>Senha:<br/>
					<input type="password" name="senha" required></p>
					<button type="submit" text="Entrar">Entrar</button>
					<?php 
                
					if(session_status()=== PHP_SESSION_NONE){ session_start();}
					if(isset($_SESSION["nome"])){
						header("location:cliente.php");
					
					}
					?>
				</form>
			</div>
		</div>
		<?php include_once 'layout/footer.php' ?>
    </div>

</body>
</html>