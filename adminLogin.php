﻿<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8" />
	<title>Estúdio Fotográfico - Admin</title>
	<link rel="stylesheet" href="conteudo/estilo.css" type="text/css" />  
	
</head>

<body>

    <div id="container">

		<?php include_once 'layout/topo.php' ?>
		<?php include_once 'layout/lateral.php' ?>
			
        <div id="conteudo">
			<div id="loginBox">
				<h3>Administrativo</h3>
				<form method="POST" action="login/verificarlogin2.php">
					<p>Usuário:<br/>
					<input type="text" name="usuario" required></p>
					<p>Senha:<br/>
					<input type="password" name="senha" required></p>
					<button type="submit" text="Entrar">Entrar</button>
				</form>
				
				<?php 
                
					if(session_status()=== PHP_SESSION_NONE){ session_start();}
					if(isset($_SESSION["admin"])){
						header("location:adminCadastro.php");
					
					}
					?>
			</div>
		</div>
		<?php include_once 'layout/footer.php' ?>
    </div>

</body>
</html>