﻿<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8" />
	<title>Estúdio Fotográfico - Admin</title>
	<link rel="stylesheet" href="conteudo/estilo.css" type="text/css" />  
	
</head>

<body>

    <div id="container">

		<?php include_once 'layout/topo.php';
			  include_once 'layout/lateral.php' ;
		
		include_once 'conexao_bd.php';
		include_once 'login/validarusuario2.php';

		if(isset($_GET["acao"]))
		{
			if(($_GET["acao"] === "e")&&(isset($_GET["id"]))) //excluir
			{
				$sql = "delete from cliente where codigo=".$_GET["id"];
				if (mysqli_query($con,$sql)){ 
					$msg = "Cliente excluído com sucesso";
					 mysqli_commit($con);                    
				}else{
					$msg = "Erro ao excluir cliente. Operação não foi efetuada";
					mysqli_rollback($con);
				}
			}
			
			if(($_GET["acao"] === "c")&&(isset($_POST))) //cadastrar
			{
				
				$sql = "insert into cliente values(null,'".$_POST["nome"]."','".$_POST["login"]."',md5('".$_POST["senha"]."'),'".$_POST["email"]."','".$_POST["dataevento"]."',NOW())";
				if (mysqli_query($con,$sql)){ 
					$msg = "Cliente excluído com sucesso";
					 mysqli_commit($con);                    
				}else{
					$msg = "Erro ao excluir cliente. Operação não foi efetuada";
					mysqli_rollback($con);
				}
				
			}
			
		}
		
		
        $sql = "select * from cliente";
		$result = mysqli_query($con,$sql);
		$qtdregistros = mysqli_num_rows($result);
		?>
		
			
        <div id="conteudo">
			<div id="cadastroBox">
				<h3>Cadastrar cliente</h3>
				<table class="tblClientes">
						<tr>
							<td>Código</td>
							<td>Nome</td>
							<td>Login</td>
							<td>Email</td>
							<td>Data do evento</td>
							<td>Data de Cadastro</td>
							<td>Ações</td>
						</tr>
						<?php
							while($row = mysqli_fetch_array($result))
							{
								echo "<tr>";
								echo "<td>".$row["codigo"]."</td>";
								echo "<td>".$row["nome"]."</td>";
								echo "<td>".$row["usuario"]."</td>";
								echo "<td>".$row["email"]."</td>";
								echo "<td>".date_format(date_create($row["dtevento"]),"d/m/Y")."</td>";
								echo "<td>".date_format(date_create($row["datagravacao"]),"d/m/Y")."</td>";
								echo "<td><a href='adminCadastro.php?acao=e&id=".$row["codigo"]."'>Excluir</a> | <a href='adminUpload.php?usu=".$row["usuario"]."&id=".$row["codigo"]."'>upload</a></td>";
								echo "</tr>";
							}
						?>
						<tr><td colspan="7">Usuarios Cadastrados: <?php echo $qtdregistros ?></td></tr>
				</table>
				
				<form method="post" action="adminCadastro.php?acao=c">
					<fieldset style="width:100%;position:relative;float:left";>
						<legend>Cadastrar novo cliente</legend>
						<p>
						Nome:
						<input type="text" name="nome" required><br/>
						Login:
						<input type="text" name="login" required><br/>
						Senha:
						<input type="password" name="senha" required><br/>
						Email:
						<input type="email" name="email" required><br/>
						Data do evento:
						<input type="date" name="dataevento" required></p>
						<input type="submit" value="Cadastrar"/>
						<p><?php if(isset($_GET["msg"]))echo $_GET["msg"]?></p>
					</fieldset>
				</form>
			</div>
		</div>
		<?php include_once 'layout/footer.php' ?>
    </div>

</body>
</html>