﻿<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8" />
	<title>Estúdio Fotográfico </title>
	<link rel="stylesheet" href="conteudo/estilo.css" type="text/css" />  

	<script src="//ajax.googleapis.com/ajax/libs/jquery/1.8.2/jquery.min.js"></script>
	<script src="js/aslider.js" type="text/javascript"></script>
    
	        
    
	
</head>

<body>

    <div id="container">

		<?php include_once 'layout/topo.php' ?>
		<?php include_once 'layout/lateral.php' ?>
			
        <div id="conteudo">
			<div id="casamentos" class="imagemconteudohome">
				<a href="casamentos.php" alt= "Link para a galeria de casamento"> <img src = "conteudo/casamento/c1.jpg" width="500"/> </a>		
			</div>
			
			<div id="ensaios" class="imagemconteudohome">
				<a href="ensaios.php" alt= "Link para a galeria de ensaio"> <img src = "conteudo/ensaio/e1.jpg" width="500"/> </a> 	
			</div>
			
			<div id="blog" class="imagemconteudohome">
				<a href="blog.php" alt= "Link para o blog"> <img src = "conteudo/casamento/c1.jpg" width="330"/> </a> 
			</div>
			
			<div id="faq" class="imagemconteudohome">
				<a href="faq.php" alt= "Link para a página de perguntas frequentes"> <img src = "conteudo/casamento/c1.jpg" width="330"/> </a> 
			</div>
			
			<div id="orcamento" class="imagemconteudohome">
				<a href="orcamento.php" alt= "Link para a página de orçamento"> <img src = "conteudo/casamento/c1.jpg" width="330"/> </a> 	
			</div>
			
			<div id="newborn" class="imagemconteudohome">
				<a href="newborn.php" alt= "Link para a galeria de recém nascido"> <img src = "conteudo/newborn/n1.jpg" width="500"/> <a> 					
			</div>
			
			<div id="sobre" class="imagemconteudohome aslider" data-hide-controls>
				<div class="aslide" data-duration="4"><img src="conteudo/home/s2.jpg" alt="Imagem - galeria2" width='500'/></div>
				<div class="aslide" data-duration="4"><img src="conteudo/home/s3.jpg" alt="Imagem - galeria3" width='500'/></div>
				<div class="aslide" data-duration="4"><img src="conteudo/home/s4.jpg" alt="Imagem - galeria4" width='500'/></div>
							
			</div>	

				
		</div>
		<?php include_once 'layout/footer.php' ?>
		</div>
    </div>

</body>
</html>