drop database estudio;

create database estudio;

use estudio;

create table cliente(
    codigo int primary key auto_increment,
    nome varchar(100),
	usuario varchar(100),
	senha varchar(32),
    email varchar(100),
    dtevento date,
    datagravacao date
);

create table cliente_imagem(
    idimagem int primary key auto_increment,
    idcliente int,
    path varchar(255),
    foreign key(idcliente) references cliente(codigo)
);

create table usuario(
    idusuario int primary key auto_increment,
    nome varchar(30),
    login varchar(100) unique,
    senha varchar(32)
);
# md5() -> função de criptografia. Gera uma chave de 32 caracteres
# unique -> o próprio mysql recursa um registro duplicado


insert into cliente values (null,'Samira','samira1',md5('123'),'teste@teste.com','2015-05-08','2015-06-19');
insert into cliente values (null,'Samira','samira2',md5('123'),'teste@teste.com','2015-06-08','2015-06-19');

insert into cliente_imagem values(null,1,'samira1/foto1.jpg');
insert into cliente_imagem values(null,1,'samira1/foto2.jpg');
insert into cliente_imagem values(null,1,'samira1/foto3.jpg');

insert into usuario values(null,'Admin','samira',md5('123'));


select * from usuario;

