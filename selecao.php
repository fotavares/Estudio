﻿<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8" />
	<title>Estúdio Fotográfico </title>
	<link rel="stylesheet" href="conteudo/estilo.css" type="text/css" />    
</head>

<body>

    <div id="container">
    
    	<?php include_once 'layout/topo.php';
		      include_once 'layout/lateral.php' ;
			  include_once 'login/validarusuario.php';
				if(!isset($_POST))
				{
					$msg="Favor selecione as imagens desejadas!";
					header("location:../cliente.php?msg=".$msg);
				}
				
			  
		?>
		<div id="conteudo">
			<h3> Obrigado! </h3>
            <p>Email enviado com sucesso!</p>
			<p>Imagens Selecionadas: <?php echo implode(", ",$_POST["cliente"])?></p>
			<a href="index.php">Voltar para a página inicial</a>
		
		
        </div>
        
        <?php include_once 'layout/footer.php' ?>
    </div>
   

</body>
</html>
