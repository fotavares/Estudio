﻿<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8" />
	<title>Estúdio Fotográfico </title>
	<link rel="stylesheet" href="conteudo/estilo.css" type="text/css" />    
</head>

<body>

    <div id="container">
    
    	<?php include_once 'layout/topo.php';
		      include_once 'layout/lateral.php' ;
			  
		?>
		<div id="conteudo">
			<h3> Obrigado! </h3>
            <p>Orçamento enviado com sucesso!</p>
			<p>Entraremos em contato em breve.</p>
			<a href="index.php">Voltar para a página inicial</a>
		
		
        </div>
        
        <?php include_once 'layout/footer.php' ?>
    </div>
   

</body>
</html>
