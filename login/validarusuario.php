<?php
    
    if(session_status()=== PHP_SESSION_NONE){ session_start();}  
    
    //ESSE ARQUIVO DEVE SER INCLUIDO EM TODAS AS PÁGINAS QUE EXIGIREM LOGIN
    //SE NÃO EXISTIR A $_SESSION["nome"] ELE SERÁ REDIRECIONADO PARA A INDEX
    if(!isset($_SESSION["nome"])){
        session_destroy();

        $msg = "Usuário não autenticado!";
        header("location:clienteLogin.php?msg=".$msg);
    }
    
    if($_SESSION["tempo"] + 2000 < time()){
        session_destroy();

        $msg = "Para ter acesso a esta área é necessário logar no Sistema";
        header("location:clienteLogin.php?msg=".$msg);
    }else{
        $_SESSION["tempo"] = time();
    }

?>
