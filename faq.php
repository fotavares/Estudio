﻿<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8" />
	<title>Estúdio Fotográfico </title>
	<link rel="stylesheet" href="conteudo/estilo.css" type="text/css" />    
</head>

<body>

    <div id="container">
    
    	<?php include_once 'layout/topo.php' ?>
		<?php include_once 'layout/lateral.php' ?>
		<div id="conteudo">
			<h3> FAQ: </h3>
    
            <ul class="faqlista1"> 
				<li>Pra lá , depois divoltis porris, paradis? </li>
				<p>Mussum ipsum cacilds, vidis litro abertis. Consetis adipiscings elitis. Pra lá , depois divoltis porris, paradis. Paisis, filhis, espiritis santis.</p>
				
				
				<li> Pra lá , depois divoltis porris, paradis? </li>
				<p>Mé faiz elementum girarzis, nisi eros vermeio, in elementis mé pra quem é amistosis quis leo. Manduma pindureta quium dia nois paga. Sapien in monti palavris qui num significa nadis i pareci latim.</p>
				
				<li> Pra lá , depois divoltis porris, paradis? </li>
				<p>Interessantiss quisso pudia ce receita de bolis, mais bolis eu num gostis?</p>
			</ul>
        </div>
        
        <?php include_once 'layout/footer.php' ?>
    </div>
   

</body>
</html>
