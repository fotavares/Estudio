﻿<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8" />
	<title>Estúdio Fotográfico </title> 
	<link rel="stylesheet" href="conteudo/estilo.css" type="text/css" />
    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.8.2/jquery.min.js"></script>
	<script src="js/aslider.js" type="text/javascript"></script>
</head>

<body>

    <div id="container">
    
    	<?php include_once 'layout/topo.php' ?>
		<?php include_once 'layout/lateral.php' ?>
		<div id="conteudo">
	      <div id="galeria">
			<div id="slider" class="aslider"  style="float:left;height:780px;"data-hide-controls>
				<div class="aslide" data-duration="4"><img src="conteudo/casamento/c1.jpg" alt="Imagem - galeria1" width='1100'/></div>
				<div class="aslide" data-duration="4"><img src="conteudo/casamento/c2.jpg" alt="Imagem - galeria2" width='1100'/></div>
				<div class="aslide" data-duration="4"><img src="conteudo/casamento/c3.jpg" alt="Imagem - galeria3" width='1100'/></div>
				<div class="aslide" data-duration="4"><img src="conteudo/casamento/c4.jpg" alt="Imagem - galeria4" width='1100'/></div>
				<div class="aslide" data-duration="4"><img src="conteudo/casamento/c5.jpg" alt="Imagem - galeria5" width='1100'/></div>
				<div class="aslide" data-duration="4"><img src="conteudo/casamento/c6.jpg" alt="Imagem - galeria6" width='1100'/></div>
				<div class="aslide" data-duration="4"><img src="conteudo/casamento/c7.jpg" alt="Imagem - galeria7" width='1100'/></div>
				<div class="aslide" data-duration="4"><img src="conteudo/casamento/c8.jpg" alt="Imagem - galeria8" width='1100'/></div>
			</div>
		 </div>
	   
	   </div>	
       <?php include_once 'layout/footer.php' ?>
	</div>
</body>
</html>
